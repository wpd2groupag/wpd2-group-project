var express = require("express");
var router = express.Router();
var Projects = require("../models/projects");
var User = require("../models/users");

// POST /add-user
router.post("/add-user/:id", (req, res, next) => {

  if (req.body.email) {
    User.findOneAndUpdate(
      { email: req.body.email },
      { $addToSet: { projects: req.params.id } },
      (error, user) => {
        if (error || !user) {
          console.log('username not found');
          res.redirect(req.get("referer"));

        } else {
          Projects.findOneAndUpdate(
            { _id: req.params.id },
            { $addToSet: { members: user._id } },
            (error, success) => {
              if (error) {
                console.log(error);
              } else {
                console.log(success);
                res.redirect(req.get("referer"));
              }
            }
          );
        }
      }
    );
  } else {
    // implement missing form logic
    res.redirect(req.get("referer"));
  }




});

module.exports = router;
