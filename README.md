# Web Platform Development 2

A web application for tracking projects and tasks. The application allows a user to Register an Account and Sign In. Once an account has been created, the application offers the user basic CRUD functionality for both Projects and the Tasks they contain. A user can Add a new Project in the dashboard, providing a project title and description. This project can then be opened, and a list of Tasks added to it, with descriptions and deadlines. Each task can be marked as completed on a specified date, edited, or deleted. A user can also share a project of theirs with another user, by adding their username to the project members.

# Development

The application was developed with [Express.js](https://expressjs.com/), a web framework built on [Node.js](https://nodejs.org/en/), a JavaScript runtime environment. The application also uses a selection of third-party middleware, in the form of [npm](https://www.npmjs.com/) packages. These packages were used to add functionality such as user authentication, session management, password encryption, and data storage.

# Install

To install the application, clone the repository and follow these steps:

    $ cd wpd2-group-project
    $ npm install
    $ npm run dev

You can then open your web browser and navigate to port 3000 on your localhost to view and test the application.

# Deployment

The application has also been deployed to [Heroku](https://dashboard.heroku.com/), where it can be accessed and tested in full without the need to download or clone.

Click [HERE](https://sheltered-headland-27030.herokuapp.com/) to go to the site.

# Credits

 - [Ross Crawford](https://bitbucket.org/ross-crawford/)
 - [Gavin Ross](https://bitbucket.org/girvain/)
 - [Steven Wilson](https://bitbucket.org/swil117/)
